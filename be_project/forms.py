from django import forms


class ContactForm(forms.Form):

    question = forms.CharField(
        max_length=2000,
        widget=forms.Textarea(
            attrs={
                'style': 'overflow: hidden; word-wrap: break-word; resize: none; height: 200px; ',
                'placeholder': 'Enter the question...',
                'id': 'text',
            }
        )
    )
    ref_answer = forms.CharField(
        max_length=2000,
        widget=forms.Textarea(
            attrs={
                'style': 'overflow: hidden; word-wrap: break-word; resize: none; height: 200px; ',
                'placeholder': 'Enter reference answer...',
                'id': 'text',
            }
        )
    )
    stud_answer = forms.CharField(
        max_length=2000,
        widget=forms.Textarea(
            attrs={
                'style': 'overflow: hidden; word-wrap: break-word; resize: none; height: 200px; ',
                'placeholder': 'Enter student answer...',
                'id': 'text',
            }
        )
    )
