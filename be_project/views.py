# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .forms import ContactForm
import globalfile
import sys


def front_page(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            question = request.POST['question']
            print question
            ref_answer = request.POST['ref_answer']
            stud_answer = request.POST['stud_answer']

            globalfile.question = question
            globalfile.ref_answer = ref_answer
            globalfile.student_response = stud_answer

            sys.path.append('/home/aditya/PycharmProjects/final-year-project/short-answer-grader/')
            import pretrainedGrader
            execfile(pretrainedGrader.py)


    else:
        form = ContactForm()
    return render(request, 'be_project/index.html', {'form': form})
