from sklearn.cluster import KMeans
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score


def train_model(features,score):
    #f1,f2,f3,f4=features
    #print features
    #train,test,trainlbl,testlbl=train_test_split(features,score,test_size=0.1,random_state=42)
    gnb = GaussianNB()

    # Train our classifier
    model = gnb.fit(features,np.asarray(score,dtype=np.int))
    return model

def predict(model, xs):
    return model.predict(xs)
