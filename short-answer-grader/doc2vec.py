import gensim
from os import listdir
from os.path import isfile, join
from scipy import spatial

LabeledSentence = gensim.models.doc2vec.LabeledSentence

class LabeledLineSentence(object):
    def __init__(self, filename):
        self.doc_list = filename
    def __iter__(self):
    	for i in range(3):
	        for uid, line in enumerate(open(filename[i])):
	        	yield LabeledSentence(words=line.split(), tags=['SENT_%s_%s' % (i,uid)])
        		print (str(i) + " ___ " +str(uid))

docLabels = ['1.1','1.2','1.3','1.4','1.5','1.6','1.7','2.1','2.2','2.3','2.4','2.5','2.6','2.7','3.1','3.2','3.3','3.4','3.5','3.6','3.7','4.1','4.2','4.3','4.4','4.5','4.6','4.7','5.1','5.2','5.3','5.4','6.1','6.2','6.3','6.4','6.5','6.6','6.7','7.1','7.2','7.3','7.4','7.5','7.6','7.7','8.1','8.2','8.3','8.4','8.5','8.6','8.7','9.1','9.2','9.3','9.4','9.5','9.6','9.7','10.1','10.2','10.3','10.4','10.5','10.6','10.7','11.1','11.2','11.3','11.4','11.5','11.6','11.7','11.8','11.9','11.10','12.1','12.2','12.3','12.4','12.5','12.6','12.7','12.8','12.9','12.10']
filename = []
for doc in docLabels:
  filename.append('/home/aditya/PycharmProjects/short-answer-grader/Train Data/' + doc)

sentence = LabeledLineSentence(filename)
model = gensim.models.Doc2Vec(size=300, window=10, min_count=5, workers=11,alpha=0.025, min_alpha=0.025) # use fixed learning rate
model.build_vocab(sentence)

for epoch in range(10):
    model.train(sentence,total_examples = model.corpus_count,epochs=model.iter)
    model.alpha -= 0.002 # decrease the learning rate
    model.min_alpha = model.alpha # fix the learning rate, no deca
    model.train(sentence,total_examples = model.corpus_count,epochs=model.iter)

model.save('doc2vec.model')

print (model.docvecs.most_similar("SENT_1_0"))
print (1 - spatial.distance.cosine(model["SENT_1_0"],model["SENT_1_3"]))